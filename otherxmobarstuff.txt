-- [ ("clear", "")
         -- , ("sunny", "")
         -- , ("mostly clear", "")
         -- , ("mostly sunny", "")
         -- , ("partly sunny", "")
         -- , ("fair", "")
         -- , ("cloudy","")
         -- , ("overcast","")
         -- , ("partly cloudy", "")
         -- , ("mostly cloudy", "")
         -- , ("considerable cloudiness", "")]

-- , "--"
-- , "-A" , "30"  -- units : °C
-- , "-L" , "44"  -- units : °C
-- , "-H" , "59"  -- units : °C
-- , "-a" , "<fn=1></fn>"
-- , "--lows" , "<fn=1></fn>"
-- , "--mediums" , "<fn=1></fn>"
-- , "--highs" , "<fn=1></fn>"

-- volume monitor
        , Run Alsa "default" "Master" [ "-t" , "<fc=#B5FF6C> | <status></fc>"
                                                , "--"
                                                , alsactl=/usr/bin/alsactl :
                                                , "-O" , " <volume>%"
                                                , "-A" , "0"  -- units: %
                                                , "-L" , "21" -- units: %
                                                , "-H" , "49" -- units: %
                                                , "-o" , "<fn=1></fn> <volume>%"
                                                , "-a" , "<fn=1></fn>"
                                                , "-l" , "<fn=1></fn>"
                                                , "-m" , "<fn=1></fn>"
                                                , "-h" , "<fn=1></fn>"
                                      ]
